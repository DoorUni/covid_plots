import sys, pickle
import glob, os
from plotTrend import *
from plotComparison import *
from rtCalculus import seir_method_rt, cori_rt, cori_rt_direct
import datetime
import seaborn as sns
import collections
from pprint import pprint
import csv

def load_dump(fileName):
    with open(fileName, "rb") as f:
        try:
            return pickle.load(f)
        except:
            print("Error read " + file)
            return None

# smooth the infected series with a moving window
def sma_infected(new_infected, window_size = 14, shift = 1):
    avg_new_infected = []
    for i in range(0, len(new_infected) - window_size + 1, shift):
        window_values = new_infected[i: i + window_size]
        avg_new_infected.append(average(window_values))

    return avg_new_infected

# calcola rt ratio rispetto a last_open_day_index, ultimo giorno compreso
def calculare_rt_ratio(last_open_day_index, last_close_day_index, rt):
    rt_0 = rt[last_open_day_index]
    #print("start rt", rt_0)
    #print("end rt", rt[last_close_day_index])

    ratio = []
    for i in range(last_open_day_index + 1, last_close_day_index + 1):
        current_ratio = rt[i] / rt_0
        ratio.append(current_ratio)

    return ratio


def init_rt_plot(simus, simu_real_clean, cut, field, title, social_distance_strictness, initial_day_restriction):
    base = datetime.date(2020, 8, 14)
    rt_list = {}
    for k in simus:
        infection_series = simus[k][field][cut:]
        date_list = [base + datetime.timedelta(days = x) for x in range(len(infection_series))]
        #rt_list[k] = cori_rt(infection_series, date_list)
        rt_list[k] = cori_rt_direct(infection_series, date_list)
        #print(k)
        #print(rt_list[k])

    if simu_real_clean != []:
        rt_list['real'] = cori_rt(simu_real_clean, date_list)
        #print('real')
        #print(rt_list['real'])

    # calcolo di R(ratio), differenza tra ultimo giorno prima della restrizione e giorno durante la restrizione
    # utilizzo i giorni per comodità, perchè gli rt hanno dei valori in meno

    rt_ratio_restriction1 = {}
    rt_ratio_restriction2 = {}
    rt_ratio_relax = {}
    for k in rt_list:
        rt = rt_list[k]['Q0.5'].tolist()
        # aggiungo i valori iniziali perchè rt cacolato da qualche giorno dopo
        while len(rt) < 201:
            rt.insert(0, 1)
        # prima restrizione
        rt_ratio_restriction1[k] = calculare_rt_ratio(39, 80, rt)
        rt_ratio_restriction2[k] = calculare_rt_ratio(99, 140, rt)
        # primo rilassamento
        rt_ratio_relax[k] = calculare_rt_ratio(140, 200, rt)


    with open('restriction1.csv', mode='w') as restriction_file:
        writer = csv.writer(restriction_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        #writer.writerow(['Nome', 'giorni'])

        start = 39
        for k in rt_ratio_restriction1:
            current = [k]
            rt = rt_list[k]['Q0.5'].tolist()
            while len(rt) < 201:
                rt.insert(0, 1)
            for i in range(0, len(rt_ratio_restriction1[k]), 7):
                current_ratio = round(rt_ratio_restriction1[k][i-1], 2)
                current_rt = round(rt[start + i], 2)
                current.append(str(current_ratio) + " (" + str(current_rt) + ")")

            writer.writerow(current)

    start = 99
    with open('restriction2.csv', mode='w') as restriction_file:
        writer = csv.writer(restriction_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        #writer.writerow(['Nome', 'giorni'])

        for k in rt_ratio_restriction2:
            current = [k]
            rt = rt_list[k]['Q0.5'].tolist()
            while len(rt) < 201:
                rt.insert(0, 1)
            for i in range(0, len(rt_ratio_restriction2[k]), 7):
                current_ratio = round(rt_ratio_restriction2[k][i-1], 2)
                current_rt = round(rt[start + i], 2)
                current.append(str(current_ratio) + " (" + str(current_rt) + ")")

            writer.writerow(current)

    start = 100
    with open('relax.csv', mode='w') as restriction_file:
        writer = csv.writer(restriction_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        #writer.writerow(['Nome', 'giorni'])

        for k in rt_ratio_relax:
            current = [k]
            rt = rt_list[k]['Q0.5'].tolist()
            while len(rt) < 201:
                rt.insert(0, 1)
            for i in range(0, len(rt_ratio_relax[k]), 7):
                current_ratio = round(rt_ratio_relax[k][i-1], 2)
                current_rt = round(rt[start + i], 2)
                current.append(str(current_ratio) + " (" + str(current_rt) + ")")

            writer.writerow(current)

    #pprint(rt_ratio_restriction)
    #pprint(rt_ratio_relax)

    plot_rt(rt_list, title, simus)
    plot_ratio(rt_ratio_restriction1, 'prima restrizione', simus)
    plot_ratio(rt_ratio_restriction2, 'seconda restrizione', simus)
    plot_ratio(rt_ratio_relax,  'rilassamento', simus)

    '''
    ratio_days = []
    ratio_days.append([0.8, 0.76, 0.72, 0.73, 0.67, 0.58, 0.51, 0.38, 0.31])
    ratio_days.append([0.82, 0.85, 0.84, 0.75, 0.67, 0.67, 0.62, 0.56, 0.5])
    ratio_days.append([0.79, 0.72, 0.71, 0.74, 0.68, 0.64, 0.58, 0.5, 0.41])
    ratio_days.append([0.57, 0.53, 0.62, 0.61, 0.63, 0.65, 0.64, 0.54, 0.42])
    boxplot_ratio_day(ratio_days, 'Restrizioni', ['7 giorni', '14 giorni', '28 giorni', '56 giorni'])

    ratio_days = []
    ratio_days.append([1.08, 0.88, 1.17, 1.15, 1.2, 1.36, 1.48, 1.86, 2.31])
    ratio_days.append([1.12, 0.99, 0.95, 1.07, 1.05, 1.28, 1.37, 1.69, 2.03])
    ratio_days.append([1.04, 0.9, 0.96, 0.98, 0.92, 1.06, 1.11, 1.81, 1.95])
    ratio_days.append([1.04, 0.91, 0.98, 0.96, 0.9, 0.9, 0.94, 1.57, 1.88])
    ratio_days.append([1.1, 0.97, 0.97, 0.86, 0.88, 0.85, 0.89, 1.1, 1.41])
    boxplot_ratio_day(ratio_days, 'Rilassamento', ['7 giorni', '14 giorni', '28 giorni', '56 giorni', '98 giorni'])
    '''
# MAIN

cut = -89

path = 'dumps/'
simus = {}

for file in os.listdir(path):
    if file.endswith(".pickle"):
        simus[file] = load_dump(os.path.join(path + file))

simu_real = [114, 89, 8, 144, 184, 86, 47, 1, 88, 91, 78, 102, 88, 44, 9, 6, 87, 81, 89, 81, 27, 76,
                96, 53, 192, 92, 99, 7, 82, 58, 168, 159, 16, 16, 87, 153, 182, 282, 501, 587, 46, 363, 44, 1032,
                1053,1319, 1388,1463, 814,1054, 1858, 2031, 2399, 2306, 2589, 2023, 194, 2708, 3211, 3979, 373,
                3695, 2242, 2829, 3613, 3654, 4296, 452, 2956, 2225, 3336, 3148, 4066, 4451, 2895, 3302, 1526,
                2356, 2565, 2928, 3706, 3232, 2208, 1604, 1442, 2261, 206, 1788, 1748]
simu_real_clean = [114, 89, 8, 144, 184, 86, 47, 1, 88, 91, 78, 102, 88, 44, 9, 6, 87, 81, 89, 81, 27, 76, 96, 53,
                    192, 92, 99, 70, 82, 58, 168, 159, 160, 160, 87, 153, 182, 282, 501, 587, 460, 363, 440, 1032, 1053, 1319, 1388, 1463,
                    814, 1054, 1858, 2031, 2399, 2306, 2589, 2023, 1940, 2708, 3211, 3979, 3730, 3695, 2242, 2829, 3613, 3654,
                    4296, 4520, 2956, 2225, 3336, 3148, 4066, 4451, 2895, 3302, 1526, 2356, 2565, 2928, 3706, 3232, 2208, 1604, 1442, 2261, 2060, 1788, 1748]
# ATTENZIONE: new_infected ha un elemento in meno

sns.set_theme()

for k in simus:
    avg_new_infected = sma_infected(simus[k]['new_infected'][0:])
    simus[k]['avg_new_infected'] = avg_new_infected

avg_real = sma_infected(simu_real_clean)

simus = collections.OrderedDict(sorted(simus.items()))

initial_day_restriction = {}
social_distance_strictness = {}
for k in simus:
    if k != "senza_restrizioni.pickle":
        initial_day_restriction[k] = simus[k]['parameters']['initial_day_restriction']
        social_distance_strictness[k] = simus[k]['parameters']['social_distance_strictness']
        base = datetime.date(2020, 8, 14)

    #initial_day_restriction = [0, 47, 107, 127, 150, 194]
    #social_distance_strictness = [0, 80, 0, 80, 0]

# Non usati per ora
#---plot_real_trend_infected(simus, [], 0)
#---plot_infected_from_test(simus, [], 0)
#---plot_growth_rate_infected(simus, [], 5)
#---plot_growth_rate_tested(simus, [], 5)
#---plot_comparison_states(simus)
#---bar_comparison_states(simus)
#---plot_comparison_tested(simus)
#---plot_single_state(simus, 'D')

plot_trend_infected_subplot(simus, [], 0)
#plot_comparison_infe_positive_subplot(simus, [], 0)
#plot_tested_rate(simus, [], 0)
#plot_positive_test_subplot(simus, [], 0)
#plot_compare_total_infected(simus)
#plot_compare_total_death(simus)

#names = ['Random', 'Sample Rank']
#times = [1.81, 443]
#plot_elapsed_time(times, names)

# stima di rt con i casi totali
#init_rt_plot(simus, [], 0, 'new_infected', "Infetti totali", social_distance_strictness, initial_day_restriction)

#plot_sds_trend(social_distance_strictness, initial_day_restriction, base, "double")

'''
initial_day_restriction = {}
social_distance_strictness = {}

restriction_days = [0, 40, 100, 200]

# sds trend single restriction
initial_day_restriction['Simu1'] = restriction_days
social_distance_strictness['Simu1'] = [10, 10, 10]

initial_day_restriction['Simu2'] = restriction_days
social_distance_strictness['Simu2'] = [10, 20, 10]

initial_day_restriction['Simu3'] = restriction_days
social_distance_strictness['Simu3'] = [10, 30, 10]

initial_day_restriction['Simu4'] = restriction_days
social_distance_strictness['Simu4'] = [10, 40, 10]

initial_day_restriction['Simu5'] = restriction_days
social_distance_strictness['Simu5'] = [10, 50, 10]

initial_day_restriction['Simu6'] = restriction_days
social_distance_strictness['Simu6'] = [10, 60, 10]

initial_day_restriction['Simu7'] = restriction_days
social_distance_strictness['Simu7'] = [10, 70, 10]

initial_day_restriction['Simu8'] = restriction_days
social_distance_strictness['Simu8'] = [10, 80, 10]

initial_day_restriction['Simu9'] = restriction_days
social_distance_strictness['Simu9'] = [10, 90, 10]

plot_sds_trend(social_distance_strictness, initial_day_restriction, base, "single")

# sds trend scala restriction
restriction_days = [0, 40, 60, 80, 100, 200]

initial_day_restriction['Simu1'] = restriction_days
social_distance_strictness['Simu1'] = [10, 90, 60, 30, 10]

initial_day_restriction['Simu2'] = restriction_days
social_distance_strictness['Simu2'] = [10, 80, 60, 40, 10]

initial_day_restriction['Simu3'] = restriction_days
social_distance_strictness['Simu3'] = [10, 50, 40, 30, 10]

initial_day_restriction['Simu4'] = restriction_days
social_distance_strictness['Simu4'] = [10, 80, 70, 60, 10]

initial_day_restriction['Simu5'] = restriction_days
social_distance_strictness['Simu5'] = [10, 30, 60, 90, 10]

initial_day_restriction['Simu6'] = restriction_days
social_distance_strictness['Simu6'] = [10, 40, 60, 80, 10]

initial_day_restriction['Simu7'] = restriction_days
social_distance_strictness['Simu7'] = [10, 30, 40, 50, 10]

initial_day_restriction['Simu8'] = restriction_days
social_distance_strictness['Simu8'] = [10, 60, 70, 80, 10]

plot_sds_trend(social_distance_strictness, initial_day_restriction, base, "scala")

# sds trend double restriction
restriction_days = [0, 40, 80, 100, 140, 200]

initial_day_restriction['Simu1'] = restriction_days
social_distance_strictness['Simu1'] = [10, 20, 0, 20, 10]

initial_day_restriction['Simu2'] = restriction_days
social_distance_strictness['Simu2'] = [10, 30, 0, 30, 10]

initial_day_restriction['Simu3'] = restriction_days
social_distance_strictness['Simu3'] = [10, 40, 0, 40, 10]

initial_day_restriction['Simu4'] = restriction_days
social_distance_strictness['Simu4'] = [10, 50, 0, 50, 10]

initial_day_restriction['Simu5'] = restriction_days
social_distance_strictness['Simu5'] = [10, 60, 0, 60, 10]

initial_day_restriction['Simu6'] = restriction_days
social_distance_strictness['Simu6'] = [10, 70, 0, 70, 10]

initial_day_restriction['Simu7'] = restriction_days
social_distance_strictness['Simu7'] = [10, 80, 0, 80, 10]

initial_day_restriction['Simu8'] = restriction_days
social_distance_strictness['Simu8'] = [10, 90, 0, 90, 10]

plot_sds_trend(social_distance_strictness, initial_day_restriction, base, "double")
'''
