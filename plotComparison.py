import matplotlib.pyplot as plt
import sys, pickle
import numpy as np
import datetime
from matplotlib.lines import Line2D

colors = ['tab:orange', 'tab:blue', 'tab:green', 'tab:red', 'tab:purple', 'tab:cyan', 'tab:pink', 'tab:brown', 'tab:olive','firebrick']

def get_infected(dump):
	# extract current infected
	infected = []

	for i in range(0, len(dump['I'])):
		infected.append(dump['I'][i] + dump['E'][i])

	return infected

def load_dump(fileName):
	with open(fileName, "rb") as f:
	    try:
	        return pickle.load(f)
	    except:
	        print("Error read " + file)
	        return None

def plot_single_state(simus, state):
	fig = plt.figure()

	i = 0

	plt.yscale('linear')

	for k in simus:
		label = k.split(".")[0]
		label = label.split("_")[-1]
		label = "sds_" + str(label)
		plt.plot(simus[k][state], color = colors[i], label = label)

		i += 1

	plt.title("Comparison state " + str(state))
	plt.xlabel("Days")
	plt.ylabel("Count")
	plt.legend()
	plt.savefig("plots/single_state_" + str(state) + ".png")
	plt.savefig("plots/single_state_" + str(state) + ".pdf")

# plot comparison of (I + E), R and D states
def plot_comparison_states(simus):
	#fig = plt.figure()
	#ax = fig.add_subplot(1, 1, 1)

	fig, axs = plt.subplots(1, 3, sharey = 'all')

	'''plt.subplots_adjust(left  = 0,  # the left side of the subplots of the figure
					right = 0.1,   # the right side of the subplots of the figure
					bottom = 0.9,  # the bottom of the subplots of the figure
					top = 0.9,      # the top of the subplots of the figure
					wspace = 0.5,   # the amount of width reserved for blank space between subplots,
					                # expressed as a fraction of the average axis width
					hspace = 0.9) '''  # the amount of height reserved for white space between subplots,
               						# expressed as a fraction of the average axis height)

	i = 0
	for key in simus:
		# extract current infected
		infected = get_infected(simus[key])
		axs[i].set_box_aspect(1)

		axs[i].plot(range(0, len(infected)), infected, color='tab:orange', label = 'Infected')
		axs[i].plot(range(0, len(infected)), simus[key]['R'], color='tab:blue', label = 'Recovered')
		axs[i].plot(range(0, len(infected)), simus[key]['D'], color='tab:red', label = 'Deaths')

		fig.suptitle('Comparison over days')
		axs[i].set_xlabel('Days')
		axs[i].set_ylabel('Count')
		axs[i].set_title('sds ' + str(simus[key]['parameters']['social_distance_strictness']), size= 10)

		# fill bottom
		axs[i].fill_between(range(0, len(infected)), infected, facecolor='orange', alpha=0.5)
		axs[i].fill_between(range(0, len(infected)), simus[key]['R'], facecolor='blue', alpha=0.5)
		axs[i].fill_between(range(0, len(infected)), simus[key]['D'], facecolor='red', alpha=0.5)

		i += 1

	handles, labels = axs[0].get_legend_handles_labels()
	plt.figlegend(handles, labels)
	plt.savefig("plots/plot_comparison_states.png")

# plot comparison of (I + E), R and D states
def bar_comparison_states(simus):

	#fig = plt.figure()
	#ax = fig.add_subplot(1, 1, 1)
	fig, axs = plt.subplots(1, 3, sharey = 'all')

	i = 0
	for key in simus:
		infected = get_infected(simus[key])
		width = 1
		axs[i].set_box_aspect(1)

		s = np.array(simus[key]['S'])
		inf = np.array(infected)
		r = np.array(simus[key]['R'])
		d = np.array(simus[key]['D'])

		axs[i].bar(range(0, len(s)), d, color = 'tab:red', label = 'Deaths', width = width)
		axs[i].bar(range(0, len(s)), inf, bottom = d, color = 'tab:orange', label = 'Infected', width = width)
		axs[i].bar(range(0, len(s)), r, bottom = inf + d, color = 'tab:blue', label = 'Recovered', width = width)
		axs[i].bar(range(0, len(s)), s, bottom = inf + r + d, color = 'tab:green', label = 'Susceptitle', width = width)

		title = key.split(".")[0]
		title = title.split("_")[-1]
		title = "sds_" + str(title)

		axs[i].set_title(title, size= 10)
		axs[i].set_xlabel('Days')
		axs[i].set_ylabel('Count')

		i += 1

		#axs.legend()

	fig.suptitle('Comparison over days')
	handles, labels = axs[0].get_legend_handles_labels()
	plt.figlegend(handles, labels)
	plt.savefig("plots/bar_comparison_states.png")

# plot comparison of real trend and new infected
# positivi sommersi
def plot_comparison_tested(simus):
	#fig = plt.figure()
	#ax = fig.add_subplot(1, 1, 1)
	fig, axs = plt.subplots(1, 3, sharey = 'all')

	i = 0
	for key in simus:
		axs[i].set_box_aspect(1)

		axs[i].plot(range(0, len(simus[key]['new_positive_counter'])), simus[key]['new_positive_counter'], color='tab:orange', label = 'Positives from test')
		axs[i].plot(range(0, len(simus[key]['new_positive_counter'])), simus[key]['new_infected'][1:], color='tab:blue', label = 'Real Positives')

		axs[i].set_title('sds ' + str(simus[key]['parameters']['social_distance_strictness']), size= 10)
		axs[i].set_xlabel('Days')
		axs[i].set_ylabel('Count')

		# fill bottom
		axs[i].fill_between(range(0, len(simus[key]['new_positive_counter'])), simus[key]['new_positive_counter'], facecolor='orange', alpha=0.5)
		axs[i].fill_between(range(0, len(simus[key]['new_positive_counter'])), simus[key]['new_infected'][1:], facecolor='blue', alpha=0.5)

		i += 1

	fig.suptitle('Comparison over days')
	handles, labels = axs[0].get_legend_handles_labels()
	plt.figlegend(handles, labels)
	plt.savefig("plots/plot_comparison_tested.pdf")

# plot comparison of real trend and I * E states cumulatives
# positivi sommersi
def plot_comparison_tested_cumu(dump, name):
	infected = get_infected(dump)

	fig = plt.figure()
	ax = fig.add_subplot(1, 1, 1)
	ax.plot(range(0, len(dump['positive'])), dump['positive'], color='tab:orange', label = 'Positives from test')
	ax.plot(range(0, len(dump['positive'])), infected, color='tab:blue', label = 'Real Positives')

	ax.set_title('Comparison over days')
	ax.set_xlabel('Days')
	ax.set_ylabel('Count')

	# fill bottom
	ax.fill_between(range(0, len(dump['positive'])), dump['positive'], facecolor='orange', alpha=0.5)
	ax.fill_between(range(0, len(dump['positive'])), infected, facecolor='blue', alpha=0.5)

	ax.legend()

	plt.savefig("plots/plot_comparison_tested_cumu_" + name + ".pdf")

# plot andamento sds
'''
def plot_sds_trend(social_distance_strictness, initial_day_restriction, base):
	fig = plt.figure()

	for i in initial_day_restriction:
		# converto le liste di intervalli nella serie
		sds_series = []
		print(initial_day_restriction[i])
		print(social_distance_strictness[i])
		for j in range(0, len(initial_day_restriction[i]) - 1):
			start = initial_day_restriction[i][j]
			end = initial_day_restriction[i][j + 1]

			sds = social_distance_strictness[i][j]

			current_sds = [sds] * (end - start)

			sds_series.extend(current_sds)

		plt.plot(sds_series)

	# Da sistemare
	#date_list = []

	#for i in range(1, len(initial_day_restriction[0]) - 1):
		#date_list.append(base + datetime.timedelta(days = initial_day_restriction[i]))


	plt.title("Social distance strictness over time")
	plt.xlabel("Steps")
	plt.ylabel("Social distance strictness")
	plt.ylim(0, 100)
	#plt.xticks(initial_day_restriction[1: -1], date_list, rotation=20)

	plt.savefig("plots/sds_trend.png")
'''
# Versione subplots
def plot_sds_trend(social_distance_strictness, initial_day_restriction, base, figtitle):
	fig, axs = plt.subplots(2, 3, figsize=(10, 8), sharey = 'all', sharex = 'all')

	row = 0

	# uso gli assi in lista così è più semplice accedere
	ax_list = fig.axes

	for i in initial_day_restriction:
		# converto le liste di intervalli nella serie
		sds_series = []
		#print(initial_day_restriction[i])
		#print(social_distance_strictness[i])
		for j in range(0, len(initial_day_restriction[i]) - 1):
			start = initial_day_restriction[i][j]
			end = initial_day_restriction[i][j + 1]
			sds = social_distance_strictness[i][j]
			current_sds = [sds] * (end - start)
			sds_series.extend(current_sds)

		ax_list[row].plot(sds_series)

		ax_list[row].set_yticks(social_distance_strictness[i])
		ax_list[row].set_yticklabels(social_distance_strictness[i])
		ax_list[row].set_xticks(initial_day_restriction[i])
		ax_list[row].set_xticklabels(initial_day_restriction[i])

		ax_list[row].set_ylim(0, 100)

		#ax_list[row].set_title(str(i), size= 10)
		#ax_list[row].set_xlabel('Days')
		#ax_list[row].set_ylabel('Social distance strictness')
		title = i.split(".")[0]

		ax_list[row].set_title(title, size= 10)

		row += 1

	fig.suptitle("Intensità delle restrizione nel tempo")
	plt.ylim(0, 100)

	fig.add_subplot(111, frameon=False)

	#fig.text(0.5, 0.04, 'Days', ha='center')
	#fig.text(0.04, 0.5, 'Social distance strictness', va='center', rotation='vertical')
	plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
	plt.xlabel('Giorni')
	plt.ylabel('Social distance strictness')

	#plt.figlegend(handles, labels)

	plt.savefig("plots/sds_trend_sub" + figtitle + ".png")
	plt.savefig("plots/sds_trend_sub" + figtitle + ".pdf")

def plot_compare_total_infected(simus):
	total = {}

	for key in simus:
		name = key.split(".")[0]
		total[name] = 0
		for new_infected in simus[key]['new_infected']:
			total[name] += new_infected

		total[name] = total[name] - simus[key]['parameters']['n_initial_infected_nodes']

	fig = plt.figure(figsize = (6, 6))

	bars = plt.bar(range(0, len(total)), list(total.values()))

	# colore senza restrizioni
	bars[0].set_color('red')
	# colore single
	bars[1].set_color('tab:green')
	bars[2].set_color('tab:green')
	bars[3].set_color('tab:green')
	# colore scala
	bars[4].set_color('tab:purple')
	bars[5].set_color('tab:purple')

	plt.title("Confronto infetti totali")
	plt.ylabel("Infetti totali")
	#plt.xlabel("Simulazioni")

	custom_lines = [Line2D([0], [0], color='red', lw=4)]

	plt.legend(custom_lines, ['Senza restrizione'])

	titles = list(total.keys())[1:]

	# tolgo il numeor davanti messo per ordinare i file
	for i in range(0, len(titles)):
		titles[i] = titles[i][1:]

	plt.xticks(range(1, len(titles) + 1), titles, rotation = 20)

	#categories = ['Singole', 'A scala', 'Doppie']
	#categories_x = [2, 4.5, 6.5]
	#plt.xticks(categories_x, categories)

	plt.savefig("plots/total_inf_bar.pdf")
	plt.savefig("plots/total_inf_bar.png")

def plot_compare_total_death(simus):
	total = {}

	for key in simus:
		name = key.split(".")[0]
		total[name] = simus[key]['D'][-1]
		#for new_infected in simus[key]['D']:
		#	print(new_infected)
		#	total[name] += new_infected

	fig = plt.figure(figsize = (8, 8))

	plt.bar(range(0, len(total)), list(total.values()))

	plt.title("Confronto decessi totali")
	plt.xlabel("Steps")
	plt.ylabel("Simulazioni")
	#plt.yscale('log')
	plt.xticks(range(0, len(total)), total.keys(), rotation='vertical')

	plt.savefig("plots/total_death_bar.pdf")
	plt.savefig("plots/total_death_bar.png")

def plot_elapsed_time(times, names):
	fig = plt.figure(figsize = (6, 6))

	plt.bar(range(0, len(times)), times)

	plt.title("Confronto tempi d'esecuzione")
	plt.ylabel("Tempo(s)")
	plt.xlabel("Simulazioni")
	plt.yscale('log')

	plt.xticks(range(0, len(names)), names)

	for i, v in enumerate(times):
		plt.text(i - 0.03, v + 0.2, str(v) + "(s)", color='black')

	plt.savefig("plots/elapsed_time.pdf")
	plt.savefig("plots/elapsed_time.png")
