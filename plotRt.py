import pandas as pd
import sys
import matplotlib.pyplot as plt

if len(sys.argv) != 2:
	print("Errore input")
	sys.exit()

fileName = sys.argv[1]
print("Reading", fileName)

simu_df = pd.read_csv(fileName)

simus = ['MiProvReal', 'MiProvSep_90', 'MiProvSep_80', 'MiProvSep_60', 'MiProvSep_40', 'MiProvSep_20', 'MiProvSep_0']

results = {}
results_cl = {}
for simu in simus:
	results[simu]= [] 
	results_cl[simu] = []

n_col = len(simu_df.columns)

days = []
for i, row in simu_df.iterrows():

	j = 1
	if i > 0:
		days.append(row[0])
		for key in results:
			# append to rt list
			results[key].append(float(row[j].replace(",", ".")))
			j += 1		
			# append to cl list
			new_row = []
			new_row.append(float(row[j].replace(",", ".")))
			j += 1
			new_row.append(float(row[j].replace(",", ".")))
			j += 1

			results_cl[key].append(new_row)

plt.figure()

colors = ['tab:orange', 'tab:blue', 'tab:green', 'tab:red', 'tab:purple', 'tab:cyan', 'firebrick']

i = 0

for key in results:
	upper = []
	lower = []
	for y in results_cl[key]:
		upper.append(y[1])
		lower.append(y[0])
	line, = plt.plot(results[key], '-', label = key, color = colors[i])
	plt.fill_between(range(len(results[key])), lower, upper, facecolor = colors[i], alpha = 0.5)

	i += 1

plt.legend()
plt.ylim(0, 2)
plt.xticks(range(len(days)), days)
plt.xlabel("Step")
plt.ylabel("Rt")

plt.show()
#plt.savefig("comparisonRtMiSep.png")






