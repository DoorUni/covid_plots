import epyestim
import epyestim.covid19 as covid19
import matplotlib.pyplot as plt
from scipy.stats import gamma, poisson
import numpy as np
import pandas as pd

# seir model method to calculate the reproductive number
def seir_method_rt(simu, cut):
	#transmission_rate = simu['parameters']['transmission_rate']

	transmission_rate = 0.009403430866327512
	total_population = simu['total'][0]
	mean_infection_duration = simu['parameters']['infection_duration']

	rt = []

	for susceptible in simu['S'][cut + 1:]:
		St = susceptible / total_population
		actual_rt = transmission_rate * St * mean_infection_duration
		rt.append(actual_rt * 10)
	
	return rt

def cori_rt(infection_series, date_list):
	# creo la serie storica
	data = pd.Series(infection_series, index = date_list) 

	#delay_distrb = covid19.generate_standard_infection_to_reporting_distribution()
	si_distrb_generated = covid19.generate_standard_si_distribution()

	shape_stimato = 1.87
	rate_stimato = 0.28

	my_continuous_distrb = gamma(a = shape_stimato, scale = rate_stimato)
	si_distrb = epyestim.discrete_distrb(my_continuous_distrb)
	'''
	fig, axs = plt.subplots(1, 2, figsize=(12,3))

	axs[0].bar(range(len(si_distrb)), si_distrb, width=1)
	axs[1].bar(range(len(si_distrb_generated)), si_distrb_generated, width=1)

	axs[0].set_title('Custom serial interval distribution')
	axs[1].set_title('Default serial interval distribution')
	plt.show()
	'''
	
	'''
	parametri calcolo rt
	gt_distribution (numpy.array): the generation time distribution (in days).
	delay_distribution (numpy.array): the distribution of delays (in days) from onset to reporting.
	a_prior (int): prior for the shape parameter of the Gamma distribution of the reproduction number
	b_prior (int): prior for the scale parameter of the Gamma distribution of the reproduction number
	smoothing_window (int): window size (in days) for the LOWESS smoothing of reported cases
	r_window_size (int): window size (in days) for final rolling average
	r_interval_dates (List[datetime.date]): time intervals for piece-wise constant reproduction number. If None, the time-varying reproduction number is computed instead.
	n_samples (int): number of bootstrap samples
	quantiles (Iterable[float]): quantiles of the posterior distribution of the reproduction number
	auto_cutoff (bool): estimates are only provided after 1. cumulative cases have reached at least 12, 2. at least one mean generation time has passed since the index case and 3., if applicable, at least one r_window_size has passed since the index case.
	'''

	ch_time_varying_r = covid19.r_covid(data, gt_distribution = si_distrb_generated, auto_cutoff = True)

	return ch_time_varying_r

def cori_rt_direct(infection_series, date_list):
	# creo la serie storica
	data = pd.Series(infection_series, index = date_list) 

	#delay_distrb = covid19.generate_standard_infection_to_reporting_distribution()
	si_distrb_generated = covid19.generate_standard_si_distribution()

	shape_stimato = 1.87
	rate_stimato = 0.28

	my_continuous_distrb = gamma(a = shape_stimato, scale = rate_stimato)
	si_distrb = epyestim.discrete_distrb(my_continuous_distrb)
	'''
	fig, axs = plt.subplots(1, 2, figsize=(12,3))

	axs[0].bar(range(len(si_distrb)), si_distrb, width=1)
	axs[1].bar(range(len(si_distrb_generated)), si_distrb_generated, width=1)

	axs[0].set_title('Custom serial interval distribution')
	axs[1].set_title('Default serial interval distribution')
	plt.show()
	'''
	
	# direct estimate from the infection data
	r_direct = epyestim.estimate_r.estimate_r(
		infections_ts = data, 
		gt_distribution = si_distrb_generated, 
		a_prior = 3, 
		b_prior = 1, 
		window_size = 7)

	for q in [0.025, 0.5, 0.975]:
		r_direct[f'Q{q}'] = epyestim.estimate_r.gamma_quantiles(q, r_direct['a_posterior'], r_direct['b_posterior'])
    
	return r_direct
	
	
	