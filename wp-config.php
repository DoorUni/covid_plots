<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_exercise' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7lfmbpft/Qp0M?dv8zi E;2KJU D]F?3j[_k;>(@GnNApX5%5>(UU* K)+Gq|%7j' );
define( 'SECURE_AUTH_KEY',  'gma|<HU,2H2DH]$FVCjysv:<98tBKDLB=S{uIUS(s2#H: %sO}fsPiL/:o}ksPCy' );
define( 'LOGGED_IN_KEY',    'JSb3pywaV2B[oEyl=i-:0$+F:)&GXmFrKsC,tO`OUI$qF,YY37itDFW]:FtFbSq6' );
define( 'NONCE_KEY',        '4b<~7U2M!1z?A8$J@<yj(j&:<ED[z).}NI^GTy>aq7sH9==b3c0X+`lot,@]~6yR' );
define( 'AUTH_SALT',        ' dlIWvTP)1,1?*jo~K9G*Wrn:j5oTZ3jpt[E/H>iPx5.bvwu6Ya{sQo7z~<mT>@f' );
define( 'SECURE_AUTH_SALT', 'ar[G#]~[&7b2I~@]iQ$ID(?nesT7IQ]h:BlW7Vq[ICU1ksHY>68t:&0$^2Ljhx=O' );
define( 'LOGGED_IN_SALT',   '}m2DKY_f@tXdWdogA06mLHhIJ@v*k_a*}KUCW<]T5M^a1^7%y.|*SrZ+]R#s&=xC' );
define( 'NONCE_SALT',       'itv$5ZoQYh_UGDV^F!Z{OeE`GK%.%9[(1/M*S{xYUu_ T&uIkCY~L4YJ(j9O`8!A' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
