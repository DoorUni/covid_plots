import matplotlib.pyplot as plt

colors = ['tab:orange', 'tab:blue', 'tab:green', 'tab:red', 'tab:purple', 'tab:cyan', 'tab:pink', 'tab:brown', 'tab:olive','firebrick']

def average(lst):
    return sum(lst) / len(lst)

def plot_real_trend_infected(simus, simu_real, cut):
	plot_trend_infected(simus, simu_real, cut, 'new_infected')

def plot_avg_trend_infected(simus, simu_real):
	plot_trend_infected(simus, simu_real, 0, 'avg_new_infected')

def plot_trend_infected(simus, simu_real, cut, field):
	# plot trend with infected
	plt.figure()

	i = 0

	for key in simus:
		for j in range(0, len(simus[key]['parameters']['initial_day_restriction']) - 1):
			if simus[key]['parameters']['social_distance_strictness'][j] > 10:
					plt.axvspan(
						simus[key]['parameters']['initial_day_restriction'][j],
						simus[key]['parameters']['initial_day_restriction'][j + 1],
						alpha = 0.1,
						color = 'red')

		label = key.split(".")[0]
		label = label.split("_")[-1]
		label = "sds_" + str(label)
		line, = plt.plot(simus[key][field][cut:], '-', label = key, color = colors[i])
		i += 1

	if simu_real != []:
		line, = plt.plot(simu_real, '-', label = 'Simu_real', color = colors[i])

	lgd = plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
	plt.xlabel("Step")
	plt.ylabel("Contagi")
	plt.title("Trend simu vs real")

	plt.savefig("plots/trend_infected_" + field + ".png", bbox_extra_artists=(lgd, ), bbox_inches='tight')

	# plot trend with infected found with test
	plt.figure()

def plot_trend_infected_subplot(simus, simu_real, cut):
	i = 0

	fig, axs = plt.subplots(2, 1, sharey = 'all', sharex = 'all', figsize = (8, 6))

	# uso gli assi in lista così è più semplice accedere
	ax_list = fig.axes

	for key in simus:
		if key != 'senza_restrizioni.pickle':
			line, = ax_list[i].plot(simus[key]['new_infected'][cut + 1:], '-', label = 'Restrizioni')

			# aggiungo la simulazione senza restrizioni
			if 'senza_restrizioni.pickle' in simus.keys():
				line, = ax_list[i].plot(simus['senza_restrizioni.pickle']['new_infected'][cut + 1:], '-', label = 'No restrizioni')

			for j in range(0, len(simus[key]['parameters']['initial_day_restriction']) - 1):
				# aggiungo fascia per restrizioni
				if simus[key]['parameters']['social_distance_strictness'][j] > 10:
					ax_list[i].axvspan(
								simus[key]['parameters']['initial_day_restriction'][j],
								simus[key]['parameters']['initial_day_restriction'][j + 1],
								alpha = 0.1,
								color = 'red')

			#ax_list[i].set_title("sds " + str(simus[key]['parameters']['social_distance_strictness']))
			ax_list[i].set_title(key.split(".")[0])

			i += 1

	handles, labels = ax_list[0].get_legend_handles_labels()
	plt.figlegend(handles, labels)

	fig.suptitle("Nuovi contagiati al variare delle restrizioni")
	'''
	fig.add_subplot(111, frameon=False)
	plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
	plt.xlabel('Days')
	plt.ylabel('New infected')
	'''
	fig.text(0.5, 0.04, 'Giorni', ha='center')
	fig.text(0.04, 0.5, 'Nuovi contagiati', va='center', rotation='vertical')

	plt.savefig("plots/trend_infected_sub.png")
	plt.savefig("plots/trend_infected_sub.pdf")

def plot_comparison_infe_positive_subplot(simus, simu_real, cut):
	i = 0

	fig, axs = plt.subplots(2, 3, sharey = 'all', sharex = 'all', figsize = (8, 6))

	# uso gli assi in lista così è più semplice accedere
	ax_list = fig.axes

	for key in simus:
		if key != 'senza_restrizioni.pickle':
			line, = ax_list[i].plot(simus[key]['new_infected'][cut + 1:], '-', label = 'Reali')

			# aggiungo la simulazione senza restrizioni
			line, = ax_list[i].plot(simus[key]['new_positive_counter'][cut + 1:], '-', label = 'Da tamponi')

			for j in range(0, len(simus[key]['parameters']['initial_day_restriction']) - 1):
				# aggiungo fascia per restrizioni
				if simus[key]['parameters']['social_distance_strictness'][j] > 10:
					ax_list[i].axvspan(
								simus[key]['parameters']['initial_day_restriction'][j],
								simus[key]['parameters']['initial_day_restriction'][j + 1],
								alpha = 0.1,
								color = 'red')

			#ax_list[i].set_title("sds " + str(simus[key]['parameters']['social_distance_strictness']))
			ax_list[i].set_title(key.split(".")[0])

			i += 1

	handles, labels = ax_list[0].get_legend_handles_labels()
	plt.figlegend(handles, labels)

	fig.suptitle("Contagiati reali e da tampone al variare delle restrizioni")
	'''
	fig.add_subplot(111, frameon=False)
	plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
	plt.xlabel('Days')
	plt.ylabel('New infected')
	'''
	fig.text(0.5, 0.04, 'Giorni', ha='center')
	fig.text(0.04, 0.5, 'Nuovi contagiati', va='center', rotation='vertical')

	plt.savefig("plots/comparison_real_test_sub.png")
	plt.savefig("plots/comparison_real_test_sub.pdf")

def plot_tested_rate(simus, simu_real, cut):
	i = 0

	fig, axs = plt.subplots(4, 2, sharey = 'all', sharex = 'all', figsize = (8, 6))

	# uso gli assi in lista così è più semplice accedere
	ax_list = fig.axes

	for key in simus:
		if key != 'senza_restrizioni.pickle':
			new_infected = simus[key]['new_infected'][cut + 1:]
			new_positive_counter = simus[key]['new_positive_counter']

			perc = []
			for j in range(0, len(new_infected)):
				perc.append((new_infected[j] - new_positive_counter[j]) / new_positive_counter[j])

			line, = ax_list[i].plot(perc, '-', label = 'Rapporto test e reali')

			for j in range(0, len(simus[key]['parameters']['initial_day_restriction']) - 1):
				# aggiungo fascia per restrizioni
				if simus[key]['parameters']['social_distance_strictness'][j] > 10:
					ax_list[i].axvspan(
								simus[key]['parameters']['initial_day_restriction'][j],
								simus[key]['parameters']['initial_day_restriction'][j + 1],
								alpha = 0.1,
								color = 'red')

			#ax_list[i].set_title("sds " + str(simus[key]['parameters']['social_distance_strictness']))
			ax_list[i].set_title(key.split(".")[0])

			i += 1

	#handles, labels = ax_list[0].get_legend_handles_labels()
	#plt.figlegend(handles, labels)

	fig.suptitle("Rapporto casi reali e da test")
	'''
	fig.add_subplot(111, frameon=False)
	plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
	plt.xlabel('Days')
	plt.ylabel('New infected')
	'''
	fig.text(0.5, 0.04, 'Giorni', ha='center')
	fig.text(0.04, 0.5, 'Nuovi contagiati', va='center', rotation='vertical')

	plt.savefig("plots/test_rate.png")
	plt.savefig("plots/test_rate.pdf")

def plot_positive_test_subplot(simus, simu_real, cut):
	i = 0

	fig, axs = plt.subplots(2, 3, sharey = 'all', sharex = 'all', figsize = (8, 6))

	# uso gli assi in lista così è più semplice accedere
	ax_list = fig.axes

	for key in simus:
		if key != 'senza_restrizioni.pickle':
			# aggiungo la simulazione senza restrizioni
			line, = ax_list[i].plot(simus[key]['new_positive_counter'][cut + 1:], '-', label = 'Da tamponi')

			for j in range(0, len(simus[key]['parameters']['initial_day_restriction']) - 1):
				# aggiungo fascia per restrizioni
				if simus[key]['parameters']['social_distance_strictness'][j] > 10:
					ax_list[i].axvspan(
								simus[key]['parameters']['initial_day_restriction'][j],
								simus[key]['parameters']['initial_day_restriction'][j + 1],
								alpha = 0.1,
								color = 'red')

			#ax_list[i].set_title("sds " + str(simus[key]['parameters']['social_distance_strictness']))
			ax_list[i].set_title(key.split(".")[0])

			i += 1

	handles, labels = ax_list[0].get_legend_handles_labels()
	plt.figlegend(handles, labels)

	fig.suptitle("Confronto dei nuovi positivi trovati dai tamponi al variare delle restrizioni")
	'''
	fig.add_subplot(111, frameon=False)
	plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
	plt.xlabel('Days')
	plt.ylabel('New infected')
	'''
	fig.text(0.5, 0.04, 'Giorni', ha='center')
	fig.text(0.04, 0.5, 'Nuovi contagiati', va='center', rotation='vertical')

	plt.savefig("plots/test_sub.png")
	plt.savefig("plots/test_sub.pdf")

def plot_infected_from_test(simus, simu_real, cut):
	i = 0

	for key in simus:
		for j in range(0, len(simus[key]['parameters']['initial_day_restriction']) - 1):
			if simus[key]['parameters']['social_distance_strictness'][j] > 10:
					plt.axvspan(
						simus[key]['parameters']['initial_day_restriction'][j],
						simus[key]['parameters']['initial_day_restriction'][j + 1],
						alpha = 0.1,
						color = 'red')

		label = key.split(".")[0]
		label = label.split("_")[-1]
		label = "sds_" + str(label)
		line, = plt.plot(simus[key]['new_positive_counter'][cut + 1:], '-', label = key, color = colors[i])
		i += 1

	if simu_real != []:
		line, = plt.plot(simu_real, '-', label = 'Simu_real', color = colors[i])

	lgd = plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
	plt.xlabel("Step")
	plt.ylabel("Contagi")
	plt.title("Trend simu vs real test founds")

	plt.savefig("plots/trend_tested", bbox_extra_artists=(lgd, ), bbox_inches='tight')

def plot_trend_subplots(simus, simu_real, cut):
	i = 0

	fig, axs = plt.subplots(2, 3)

	for key in simus:
		line, = axs[0].plot(simus[key]['new_infected'][cut + 1:], '-', label = key, color = colors[i])
		i += 1
	line, = axs[0].plot(simu_real, '-', label = 'Simu_real', color = colors[i])
	axs[0].set_title("Real infected")

	i = 0
	for key in simus:
		line, = axs[1].plot(simus[key]['new_positive_counter'][cut + 1:], '-', label = key, color = colors[i])
		i += 1
	line, = axs[1].plot(simu_real, '-', label = 'Simu_real', color = colors[i])
	axs[1].set_title("Tested infected")

	axs[0].set_xlabel("Step")
	axs[0].set_ylabel("Contagi")
	axs[1].set_xlabel("Step")
	fig.suptitle("Trend simu vs real test founds")

	handles, labels = axs[0].get_legend_handles_labels()
	plt.figlegend(handles, labels)
	plt.savefig("plots/trend_sub")

def plot_positive_perc(simus, cut):
	i = 0

	plt.figure()

	for key in simus:
		perc_pos = []
		n_test = simus[key]['parameters']['n_test']
		for pos in simus[key]['new_positive_counter'][cut:] :
			perc = (pos / n_test) * 100
			perc = round(perc, 2)
			perc_pos.append(perc)

		line, = plt.plot(perc_pos, '-', label = key, color = colors[i])

		i += 1

	plt.legend()
	plt.xlabel("Step")
	plt.ylabel("Percentuale")
	plt.title("Percentuale positivi su tamponi")
	plt.ylim(0, 100)

	plt.savefig("plots/plot_perc_pos.png")

def plot_growth_rate_infected(simus, simu_real, cut):
	plot_growth_rate(simus, simu_real, cut, 'new_infected')

def plot_growth_rate_tested(simus, simu_real, cut):
	plot_growth_rate(simus, simu_real, cut, 'new_positive_counter')

def plot_growth_rate(simus, simu_real, cut, field):

	growth_rate = {}
	# initialize rates

	if simu_real != []:
		growth_rate['real'] = []
		for i in range(0, len(simu_real) - 1, 1):
			growth_rate['real'].append((simu_real[i + 1] - simu_real[i]) / simu_real[i])

	for k in simus:
		growth_rate[k] = []
		new_infected = simus[k][field][cut + 1:]
		for i in range(0, len(new_infected) - 1, 1):
			growth_rate[k].append((new_infected[i + 1] - new_infected[i]) / new_infected[i])

	'''
	# stampo la serie
	print("infected")
	for k in growth_rate:
		print(k)
		print([round(num, 2) for num in growth_rate[k]])
	'''

	avg_growth_rate = {}

	# faccio la media del tasso di crescita tramite la moving window
	window_size = 21
	shift = 1
	for k in growth_rate:
		avg_growth_rate[k] = []
		for i in range(0, len(growth_rate[k]) - window_size + 1, shift):
			window_values = growth_rate[k][i: i + window_size]
			avg_growth_rate[k].append(average(window_values))

	# versione singolo plot
	plt.figure()

	i = 0

	for k in avg_growth_rate:
		for j in range(0, len(simus[k]['parameters']['initial_day_restriction']) - 1):
			if simus[k]['parameters']['social_distance_strictness'][j] > 10:
					plt.axvspan(
						simus[k]['parameters']['initial_day_restriction'][j],
						simus[k]['parameters']['initial_day_restriction'][j + 1],
						alpha = 0.1,
						color = 'red')
		label = k.split(".")[0]
		label = label.split("_")[-1]
		line, = plt.plot(avg_growth_rate[k], label = k, color = colors[i])
		#plt.ylim(-1, 2)
		i += 1

	plt.ylabel("Tasso di crescita")
	plt.xlabel("Step")
	plt.title("Tasso di crescita " + field)
	plt.legend()

	plt.savefig("plots/growth_rate_" + field + ".png")

def plot_growth_rate_trend(simu_real, simu_real_clean):

	growth_rate = []
	for i in range(0, len(simu_real) - 1):
		growth_rate.append((simu_real[i + 1] - simu_real[i]) / simu_real[i])

	fig, axs = plt.subplots(2, 1)

	handles = []
	labels = []
	line, = axs[0].plot(growth_rate, label = 'Tasso crescita', color = colors[0])
	handle, label = axs[0].get_legend_handles_labels()
	handles.append(handle)
	labels.append(label)
	line, = axs[1].plot(simu_real, label = 'Andamento', color = colors[1])
	line, = axs[1].plot(simu_real_clean, label = 'Andamento clean', color = colors[2])
	handle, label = axs[1].get_legend_handles_labels()
	handles.append(handle)
	labels.append(label)

	#plt.figlegend(handles, labels)
	axs[1].set_xlabel("Step")
	fig.suptitle("Andamento reale e tasso di crescita")

	plt.savefig("plots/plot_growth_rate_trend")

def plot_rt(rt_list, title, simus):
	i = 0
	fig, ax = plt.subplots(2, 3, figsize=(8, 6), sharex = 'all', sharey = 'all')

	ax_list = fig.axes

	rt_no_restriction = rt_list['senza_restrizioni.pickle']['Q0.5'].tolist()

	for k in rt_list:
		if k != 'senza_restrizioni.pickle':

			for j in range(0, len(simus[k]['parameters']['initial_day_restriction']) - 1):
				if simus[k]['parameters']['social_distance_strictness'][j] > 10:
						ax_list[i].axvspan(
							simus[k]['parameters']['initial_day_restriction'][j],
							simus[k]['parameters']['initial_day_restriction'][j + 1],
							alpha = 0.1,
							color = 'red')

			'''
			label = k.split(".")[0]
			label = label.split("_")[-1]
			label = "sds_" + str(label)
			'''

			ch_time_varying_r = rt_list[k]
			rt = ch_time_varying_r['Q0.5'].tolist()

			# aggiungo i valori iniziali perchè rt cacolato da qualche giorno dopo
			while len(rt) < 201:
				rt.insert(0, 1)

			#ch_time_varying_r.loc[:,'Q0.5'].plot(ax = ax_list[i], color = colors[i], label = k)
			ax_list[i].plot(rt, label = 'Restrizioni')
			# aggiungo rt senza restrizioni
			ax_list[i].plot(rt_no_restriction, label = 'No restrizioni')
			#ax_list[i].set_title("sds " + str(simus[k]['parameters']['social_distance_strictness']))
			ax_list[i].set_title(k.split(".")[0])

			'''
			ax_list[i].fill_between(ch_time_varying_r.index,
		                    ch_time_varying_r['Q0.025'],
		                    ch_time_varying_r['Q0.975'],
		                    color = 'red', alpha = 0.2)
			'''

			#ax_list[i].set_ylim([0, 2])
			ax_list[i].axhline(y = 1)

			i += 1

	fig.suptitle("R(t) al variare delle restrizioni")

	fig.text(0.5, 0.04, 'Giorni', ha='center')
	fig.text(0.04, 0.5, 'R(t)', va='center', rotation='vertical')

	#ax.set_title('time-varying effective reproduction number, ' + title)
	handles, labels = ax_list[0].get_legend_handles_labels()
	plt.figlegend(handles, labels)
	plt.savefig('plots/rt_' + title + ".png")
	plt.savefig('plots/rt_' + title + ".pdf")

'''
def plot_ratio(rt_ratio, title):
	i = 0
	fig = plt.figure()

	for k in rt_ratio:
		line, = plt.plot(rt_ratio[k], color = colors[i], label = k)

		i += 1
	plt.xlabel('date')
	plt.ylabel('R(t) ratio')
	plt.title('Rt ratio durante ' + title)
	lgd = plt.legend(bbox_to_anchor = (1.05, 1), loc = 'upper left')
	plt.savefig('plots/rt_ratio_' + title + '.png', bbox_extra_artists=(lgd, ), bbox_inches='tight')
'''
def plot_ratio(rt_ratio, title, simus):
	i = 0
	fig, ax = plt.subplots(2, 3, figsize=(8, 6), sharex = 'all', sharey = 'all')

	ratio_no_restriction = rt_ratio['senza_restrizioni.pickle']

	ax_list = fig.axes

	for k in rt_ratio:
		if k != 'senza_restrizioni.pickle':
			ax_list[i].plot(rt_ratio[k], label = 'Restrizione')
			ax_list[i].plot(ratio_no_restriction, label = 'No restrizioni')
			#ax_list[i].set_title("sds " + str(simus[k]['parameters']['social_distance_strictness']))
			ax_list[i].set_title(k.split(".")[0])

			i += 1

	fig.suptitle('Rt ratio durante ' + title)
	fig.text(0.5, 0.04, 'Giorni', ha='center')
	fig.text(0.04, 0.5, 'R(t) ratio', va='center', rotation='vertical')

	handles, labels = ax_list[0].get_legend_handles_labels()
	plt.figlegend(handles, labels)
	#lgd = plt.legend(bbox_to_anchor = (1.05, 1), loc = 'upper left')
	plt.savefig('plots/rt_ratio_' + title + '.png')
	plt.savefig('plots/rt_ratio_' + title + '.pdf')

def boxplot_ratio_day(ratio_days, title, days):
	fig = plt.figure(figsize = (8, 6))

	plt.boxplot(ratio_days, positions = range(0, len(ratio_days)))
	plt.xticks(range(0, len(ratio_days)), days)
	plt.savefig('plots/boxplot_ratio_' + title + '.png')
	plt.savefig('plots/boxplot_ratio_' + title + '.pdf')
